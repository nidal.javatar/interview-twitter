import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';
import { UserSignUpModel } from '../../models/user/user-signup.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  model: UserSignUpModel = new UserSignUpModel();
  loading = false;
  incorrectUsernameError = false;

  constructor(private userservice: UserService, private router: Router) { }

  onSubmit(signupForm: NgForm): void {
    if (signupForm.valid) {
      this.signup(signupForm);
    }
  }

  signup(signupForm: NgForm) {
    this.loading = true;
    this.userservice.saveUser(this.model).subscribe(
      (data) => {
        signupForm.resetForm();
        this.router.navigate(['/auth/login']);
      },
    );
    this.incorrectUsernameError = true;
  }

  isFormSubmittedWithInvalidUsername(signupForm: NgForm): boolean {
    const usernameFormControl = signupForm.form.controls['username'];
    return signupForm.submitted && usernameFormControl && !usernameFormControl.valid;
  }

  isFormSubmittedWithInvalidPassword(signupForm: NgForm): boolean {
    const passwordFormControl = signupForm.form.controls['password'];
    return signupForm.submitted && passwordFormControl && !passwordFormControl.valid;
  }

  isFormSubmittedWithInvalidFirstName(signupForm: NgForm): boolean {
    const usernameFormControl = signupForm.form.controls['firstname'];
    return signupForm.submitted && usernameFormControl && !usernameFormControl.valid;
  }

  isFormSubmittedWithInvalidLastName(signupForm: NgForm): boolean {
    const passwordFormControl = signupForm.form.controls['lastname'];
    return signupForm.submitted && passwordFormControl && !passwordFormControl.valid;
  }

  handleUniqueException(err: string) {
    this.incorrectUsernameError = true;
  }

}
