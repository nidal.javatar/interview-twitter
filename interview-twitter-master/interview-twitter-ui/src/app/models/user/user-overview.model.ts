export interface UserOverviewModel {
    tweetsNum: number,
    followersNum: number,
    followingNum: number
  }
  