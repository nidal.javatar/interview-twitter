import { UserModel } from "../user/user.module";

export interface TweetModel {
  id: number,
  content: string,
  author: UserModel;
}
