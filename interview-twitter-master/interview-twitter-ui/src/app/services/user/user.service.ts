import { Injectable } from "@angular/core";
import { UserOverviewModel } from "../../models/user/user-overview.model";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserModel } from "../../models/user/user.module";
import { UserSignUpModel } from "../../models/user/user-signup.model";

const ENDPOINT_BASE = '/api/user';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  fetchUserOverview(): Observable<UserOverviewModel>{
    return this.http.get<UserOverviewModel>(ENDPOINT_BASE + '/overview');
  }

  saveUser(user: UserSignUpModel): Observable<UserModel> {
      console.log("Uso")
    return this.http.post<UserModel>(ENDPOINT_BASE + '/signup', user); 
  }

}