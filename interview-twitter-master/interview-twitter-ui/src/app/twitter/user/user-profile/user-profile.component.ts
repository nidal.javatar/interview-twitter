import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user/user.service';
import { Observable } from 'rxjs';
import { UserOverviewModel } from '../../../models/user/user-overview.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  $userOverview: Observable<UserOverviewModel>;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.$userOverview = this.userService.fetchUserOverview();
  }

}
