import { UserProfileComponent } from "./user-profile/user-profile.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { UserService } from "../../services/user/user.service";
import { UserOverviewComponent } from './user-overview/user-overview.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
              path: '', component: UserProfileComponent, 
            },
          ]),
      CommonModule,
      FormsModule,
    ],
    declarations: [UserProfileComponent, UserOverviewComponent],
    providers: [UserService],
    exports: [UserProfileComponent]
  })
  export class UserModule {
  }