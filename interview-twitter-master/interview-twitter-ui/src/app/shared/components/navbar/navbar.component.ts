import {Component} from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  preserveWhitespaces: false,
})
export class NavbarComponent {

  isLogged: boolean;
  constructor(private authService: AuthService, private router: Router) {
  this.isLogged = authService.isLoggedIn();
  }

  getCurrentUser(): string {
    this.isLogged = true;
    return this.authService.getCurrentUser();
    
  }

  signOut(): void {
    this.authService.logout();
    this.router.navigate(["/auth/login"]);
  }

}
