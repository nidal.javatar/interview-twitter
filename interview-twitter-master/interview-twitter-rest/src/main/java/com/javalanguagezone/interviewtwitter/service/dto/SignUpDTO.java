package com.javalanguagezone.interviewtwitter.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor(access = PRIVATE)
public class SignUpDTO {

  private String username;
  private String password;
  private String firstname;
  private String lastname;

  public SignUpDTO(String username, String password, String firstname, String lastname) {
    this.username = username;
    this.password = password;
    this.firstname = firstname;
    this.lastname = lastname;
  }
}
