package com.javalanguagezone.interviewtwitter.service.dto;

import com.javalanguagezone.interviewtwitter.domain.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor(access = PRIVATE)
public class UserOverviewDTO {

  private Integer tweetsNum;
  private Integer followersNum;
  private Integer followingNum;

  public UserOverviewDTO(User user, Integer tweetNum) {
    this.tweetsNum = tweetNum;
    this.followersNum = user.getFollowers().size();
    this.followingNum = user.getFollowing().size();
  }

}
