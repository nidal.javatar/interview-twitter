package com.javalanguagezone.interviewtwitter.controller;

import com.javalanguagezone.interviewtwitter.service.dto.SignUpDTO;
import com.javalanguagezone.interviewtwitter.service.dto.UserDTO;
import com.javalanguagezone.interviewtwitter.service.dto.UserOverviewDTO;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class UserControllerIntegrationTest extends RestIntegrationTest {

  @Test
  public void followersRequested_allFollowersReturned() {
    ResponseEntity<UserDTO[]> response = withAuthTestRestTemplate().getForEntity("/followers", UserDTO[].class);
    assertThat(response.getStatusCode().is2xxSuccessful(), is(true));
    List<UserDTO> followers = Arrays.asList(response.getBody());
    assertThat(followers, hasSize(1));
    assertThat(extractUsernames(followers), contains("rogerkver"));
  }

  @Test
  public void getFollowingFromFirstPage() {
    ResponseEntity<UserDTO[]> response = withAuthTestRestTemplate().getForEntity("/following", UserDTO[].class);
    assertThat(response.getStatusCode().is2xxSuccessful(), is(true));
    List<UserDTO> following = Arrays.asList(response.getBody());
    assertThat(following, hasSize(4));
    assertThat(extractUsernames(following), containsInAnyOrder(followingUsers()));
  }

  @Test
  public void getUserOverview_withOutlogin() {
    ResponseEntity<UserOverviewDTO> response = withUnauthorizedUser().getForEntity("/user/overview", UserOverviewDTO.class);
    assertThat(response.getStatusCode().is4xxClientError(), is(true));
  }

  @Test
  public void userOverviewRequested_allOverviewsReturned() {
    ResponseEntity<UserOverviewDTO> response = withAuthTestRestTemplate()
      .getForEntity("/user/overview", UserOverviewDTO.class);
    assertThat(response.getStatusCode().is2xxSuccessful(), is(true));
    UserOverviewDTO overview = response.getBody();
    assertThat(overview.getFollowersNum(), equalTo(1));
    assertThat(overview.getFollowingNum(), equalTo(4));
    assertThat(overview.getTweetsNum(), equalTo(4));

  }

  @Test
  public void userRegisterSuccessForPublicUsers(){
    SignUpDTO userData = registrationData();
    ResponseEntity<UserDTO> response = withUnauthorizedUser()
      .postForEntity("/user/signup", userData, UserDTO.class);
    assertThat(response.getStatusCode().is2xxSuccessful(), is(true));
    UserDTO user = response.getBody();
    assertThat(user.getUsername(), equalTo(userData.getUsername()));
    assertThat(user.getFullName(), equalTo(String.format("%s %s", userData.getFirstname(), userData.getLastname())));
    assertThat(user.getId(), notNullValue());

  }

  @Test
  public void userRegisterSuccessWithNullFirstAndLastName(){
    SignUpDTO userData = registrationDataWithNullValues();
    ResponseEntity<UserDTO> response = withUnauthorizedUser()
      .postForEntity("/user/signup", userData, UserDTO.class);
    assertThat(response.getStatusCode().is2xxSuccessful(), is(true));
    UserDTO user = response.getBody();
    assertThat(user.getUsername(), equalTo(userData.getUsername()));
    assertThat(user.getFullName(), equalTo(" "));
    assertThat(user.getId(), notNullValue());

  }

  @Test
  public void userRegisterWithTakenUsername(){
    SignUpDTO userData = registrationData();
    ResponseEntity<UserDTO> response = withUnauthorizedUser()
      .postForEntity("/user/signup", userData, UserDTO.class);
    assertThat(response.getStatusCode().is2xxSuccessful(), is(true));
    ResponseEntity<UserDTO> responseInvalid = withUnauthorizedUser()
      .postForEntity("/user/signup", userData, UserDTO.class);
    assertThat(responseInvalid.getStatusCode().is5xxServerError(), is(true));
  }

  private List<String> extractUsernames(List<UserDTO> users) {
    return users.stream().map(UserDTO::getUsername).collect(toList());
  }

  private SignUpDTO registrationData() {
    return new SignUpDTO(String.format("test#%s", UUID.randomUUID()), "test", "test", "test");
  }

  private SignUpDTO registrationDataWithNullValues() {
    return new SignUpDTO("test#1", "test", null, null);
  }
}
